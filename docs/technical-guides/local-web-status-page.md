---
title: Local web status page
---

The local web status page of a device allows you to know its initial state as well as its connection to the API.

## First boot

The firmware does not contain any API server by default.

At the first start, it will therefore be necessary to configure the URL of the API server.

To access the configuration web page, there are 2 possible ways: 

- Open wifi SentinelC-XXXXXX
- An ethernet port 

If the device is equipped with a compatible wifi radio, an open, unprotected wifi will be exposed. 

Otherwise, the device must have **at least 2 Ethernet ports** and you must connect to the second port. 

Once logged in, open http://192.168.99.1:9888/ to access the status/configuration page. 

We then get this:

![web status](local-web-status/web-status1.png)

## Entering the API server address

Enter the API server address in the field provided.

![web status API server](local-web-status/web-status-api-server.png)

The url must start with http:// or https:// and include the domain name or IP address of the server.

Examples:

`https://api.sentinelc.com`

`https://api.dev.sentinelc.com`

`https://sentinelc.example.net`

`http://192.168.122.48:8000`

Once you have entered the API server address, click on the red Send Configuration button.
The machine will then restart on the field.
In order to check that the server is well configured, you just have to check the management console of our API server to see if the new equipment tries to connect.

If he connects for the first time, we will see his registration request appear in Django.

## Access via management VPN

A technician can access the status page via the management VPN.

The link is visible in the Remote access section of the device in the admin.

![web status django](local-web-status/web-status-django1.png)

## Access via a local management vlan

A technician can also open the status page on a given zone by checking the Management option in the vlan config in django.

By default it is always closed.

![web status django 2](local-web-status/web-status-django2.png)

## Diagnose an offline appliance during the initial setup.

In order to diagnose an appliance that appears to be offline, you need to check its local status.

To do this, make sure your SentinelC appliance is turned on, then use your mobile device to scan for Wi-Fi networks, you will then see a Wi-Fi named SentinelC followed by the serial number of your device.

To find the serial number and registration key of your appliance, the key is printed on the back of the Quick Setup Guide and on the bottom of your SentinelC router.

Select this Wi-Fi and enter your appliance's registration key as the Wi-Fi password.

Once connected to the Wi-Fi, launch your Internet browser and type in `http://192.168.99.1:9888` as the address, you will then arrive at the appliance status page and you can see its status.

![Wi-Fi Config](local-web-status/offline-appliance.png)

## Connect to the internet via a Wi-Fi link.

On your device's local status page, go to the very bottom to the **Edit WAN Connection** section, under the Wi-Fi tab. Enter the Wi-Fi information and click **Send Configuration**.

![Wi-Fi Config](local-web-status/wi-fi-config.png)


##  Connect to the internet with a static IP.

In the local status page of your device, go to the very bottom to the section **Edit WAN connection**.