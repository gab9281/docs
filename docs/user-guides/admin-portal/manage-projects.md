---
title: Manage projects
---

The projects page of the Admin portal is used to manage all projects on the SentinelC network.
With the creation of a project, it is possible to track activities related to monitoring.

The project allows you to have a global view of all the actions performed on a number of appliance and/or devices. 

It is possible to deploy and execute services on appliance that allow more monitoring on the network.


![Projects list](manage-projects/project.png)

## How to create a project?

To create a project, login to the Admin portal, then go to the projects page, then click on **Create project**.

![Create project](manage-projects/create-project.png)

Then indicate the title of the project with its summary and click on **Create project**.

## How to close a project?

To close a project, select it, then select the Close action and click **Apply**.

![Add an appliance](manage-projects/close-project.png)

Click **Confirm** to close the project.

## How to add an appliance to the project?

Your project is created, you can then add an appliance. To do so, click on **Add an appliance**.

![Add an appliance](manage-projects/add-appliance.png)

Enter the account name, its location and the appliance then click on **Confirm**.

## How to perform different operations on a project?

For perform different operations on a projet, you must first add a component.

In your project, select your equipment and click on add a component. Several choices are offered:

- Packets capture.
- Ports scan.

###  How to start Packets capture?

To start a packet capture, select an appliance, then add the packet capture component.

![Add packets capture](manage-projects/add-packets-capture.png)

Then click on **+ New packet capture** to bring up the packet capture form.

![Packet capture form](manage-projects/packet-capture-form.png)

Fill in the form fields and click **Apply** to start the packets capture.

### How to start Ports scan?

To start a ports scan, select an appliance, then add the ports scan component.

![Add ports scan](manage-projects/add-ports-scan.png)

Then click on **+ New port scan** to bring up the packet capture form.

![Ports scan form](manage-projects/ports-scan-form.png)

Fill in the form fields and click **Apply** to start the ports scan.