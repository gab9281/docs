---
title: Replace failing appliance
---

## How do I replace a failed master router?


To replace a primary router, log into the SentinelC application. Select your location and click on **Appliance**.

![Location](./installation-ap/locations.png)

On the appliance page, select your primary router and next to the appliance name to **Edit** it..

![Appliance edit](./installation-ap/appliances-edit.png)

In the information page of your master router, click on the 3 small dots at the top right to select the **Replace Master Router** function.

![select replace master](./installation-ap/select-replace-router.png)

Once in the main router replacement page, please enter the product key of the replacement device and click **Continue**.

![Replace master router](./installation-ap/replace-master-router.png)

You can then rename your new device or keep the original name. Click **Apply** to save the changes.

image

A confirmation popup appears to confirm the changes, click on **Confirm** to finalize the replacement.

image

A message appears, click on **Ok** to return to the appliance page. The application informs you that the configuration will be transferred to the new router as soon as it is connected.

image

- The operation also resets the old equipment to the factory state. You can confirm that the reset has worked by scanning the Wi-Fi with your cell phone, for example.
A factory reset device emits an initial configuration Wi-Fi named SentinelC-XXXXX. If you see this Wi-Fi, the device is de-configured.

- If the old device is failing, the factory reset may not be able to complete.

- The old device must be connected to the internet to receive the reset command.

- You can confirm the transfer of configurations to the new device in the Devices page, once your new router is connected. The new device should appear as your primary router. Also, no configuration changes should be pending.

- If a message indicates that the transfer of configurations is pending, make sure that your new equipment is connected to the Internet and wait a few minutes.
