---
title: Wi-Fi scans
---

Wi-Fi scan is used to check if a rogue Wi-Fi is detected around your network.

A rogue Wi-Fi is a wireless access point that has been installed on a secure network without explicit authorization from a local network administrator, whether added by a well-meaning employee or by a malicious attacker.


The results of Wi-Fi scans are visible in the Admin portal:

![Wi-Fi scans result](wi-fi-scan/result-wi-fi-scan.png)

## Start new Wi-Fi scan.

In order to start a Wi-FI survey with a SentinelC device, please refer to how [start Wi-Fi survey](/docs/user-guides/admin-portal/manage-appliances/operations#start-wi-fi-survey)

To start a new Wi-Fi scan, log into the Admin portal, go to Wi-Fi scan tab and click on the red **New Wi-Fi scan** button.

![Wi-Fi scans](wi-fi-scan/wi-fi-scan.png)

Fill in the fields of the form specifying the account, the location and the appliance. Click to **Apply** for start a Wi-Fi scan.

![New Wi-Fi scans](wi-fi-scan/new-wi-fi-scan.png)

A WiFi scan task is then launched.

![Wi-Fi scans pending](wi-fi-scan/wi-fi-scan-pending.png)

Once completed, go to the Wi-Fi scan page and open the Wi-Fi Scan details. Here in our example, we can see that there is a Wi-Fi rogue

![Wi-Fi scans result](wi-fi-scan/result-wi-fi-scan.png)

## View Wi-Fi rogue report.

For view Wi-Fi rogue report. Just click on **View rogue Wi-Fi report** in detail of result Wi-Fi scan. You are then redirected to the Potential rogue Wi-Fi page with report Wi-Fi rogue.

![Wi-Fi rogue](wi-fi-scan/rogue.png)

## Whitelist Wi-Fi.

Once the Wi-Fi rogue is identified, you can choose to whitelist it if it is not dangerous, or mark it as a rogue by clicking to **Apply**.

![Whitelist Wi-Fi](wi-fi-scan/whitelist-wi-fi.png)
