---
title: Inventory
---

From the Appliances main page, click the **Manage inventory** button to access your inventory.

This lists the appliances that are newly registered with the cloud controller, that are in stock or that have been returned to the inventory.

![Buttons](img/inventory-buttons.png)

## Registering an appliance to the inventory

A freshly flashed appliance running the SentinelC OS needs to be registered to your cloud controller to appear in your inventory.

Refer to the [First boot](/docs/technical-guides/local-web-status-page) documentation for details.

In order to appear in the inventory, the registration request coming from your appliance needs to originate from a known Public IP address. Open the **Inventory settings** page to configure it.

To know your public IP address, go to https://ifconfig.me/

![Manage inventory](img/manage-inventory.png)

Enter this IP in Inventory settings.

![Add IP](img/add-ip.png)

- Once the new appliance is detected in the Admin portal, add it to the inventory. To do this, select the appliance and use the **add to inventory** action.

## Adding an appliance to the inventory

A brand new appliance that has just been registered will appear with the inventory status NEW.

Likewise, an appliance that was in-use by an account but that has been Returned to the operator will appear RETURNED.

In both theses cases, you need to accept the New or Returned appliances for them to become In stock.


![Add inventory](img/add-inventory-appliance.png)

Enter this location and **Confirm** for add appliance in inventory.

![Add inventory confirm](img/add-inventory-appliance-confirm.png)


## Claiming an appliance

Once claimed, the appliance leaves the inventory and becomes "claimed" by an account (tenant). The administrators of this account will gain access to the appliance and can view its status and change its configuration.

There are multiple ways to claim an appliance:

- If enabled, an anonymous user that knows the Registration key of the appliance can perform an [Onboarding](/docs/user-guides/dashboard/sentinelc-router-initial-onboarding) to setup his account, location and configure an appliance as a router.
- Use the [Add location](/docs/user-guides/dashboard/add-location) operation in the Dashboard.
- Use the [Add an access point](/docs/user-guides/dashboard/installation-access-point) operation in the Dashboard.
- Use the Claim operation in the Admin portal.


## Transfer to

For appliances that are in the inventory, the **Transfer to** operation can be used to keep track of the physical location where your appliances are kept.

![Transfert to](img/transfert-to.png)

Select the inventory location and confirm.

![Confirm Transfert to](img/transfert-to-confirm.png)

## Switch API endpoint

If you want to change the environment of your equipment, it is possible to change the API ENDPOINT of an equipment.

This will send the appliance to be managed by another cloud controller installation.

![Switch API endpoint](img/switch-api.png)

Select the Switch API endpoint action and click Apply.

![Switch API endpoint](img/switch-api-form.png)

Enter the new url for API endpoint and you can input a reason. You can track the progression of the operation with the Task that will be created. Once completed, the appliance will change to the Unavailable status, which is filtered out by default.
