/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'SentinelC',
  tagline: 'The tagline of my site',
  url: 'https://your-docusaurus-test-site.com',
  baseUrl: process.env.BASE_URL || "/",
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon-32x32.png',
  trailingSlash: true,
  organizationName: 'facebook', // Usually your GitHub org/user name.
  projectName: 'docusaurus', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: '',
      logo: {
        alt: 'SentinelC Logo',
        src: 'img/LogoFullBlack.png',
      },
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Docs',
          position: 'left',
        },
        {to: 'blog', label: 'Blog', position: 'left'},

      ],
    },
    colorMode: {
      "defaultMode": "light",
      "disableSwitch": true,
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Présentation',
              to: 'docs/',
            },
          ],
        },
        {
          title: 'Communauté',
          items: [
            {
              label: 'Ma page 1',
              href: 'https://stackoverflow.com/questions/tagged/docusaurus',
            },
            {
              label: 'Ma page 3',
              href: 'https://discordapp.com/invite/docusaurus',
            },
            {
              label: 'Ma page 2',
              href: 'https://twitter.com/docusaurus',
            },
          ],
        },
        {
          title: 'Plus',
          items: [
            {
              label: 'FAQ',
              to: 'blog',
            },
            {
              label: 'Test',
              href: 'https://github.com/facebook/docusaurus',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} SentinelC. Tous droits réservés.`,
    },
    zoom: {
      selector: '.markdown :not(em) > img',
      config: {
        // options you can specify via https://github.com/francoischalifour/medium-zoom#usage
        background: {
          light: 'rgb(255, 255, 255)',
          dark: 'rgb(50, 50, 50)'
        }
      }
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          // editUrl:
          //  'https://github.com/facebook/docusaurus/edit/master/website/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/blog/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
  plugins: [
    require.resolve('@easyops-cn/docusaurus-search-local'),
    require.resolve('docusaurus-plugin-image-zoom')
  ],
};
