---
title: Appliance details
---

![Appliance details](img/appliance-details.png)

You can also get the history of the appliance by clicking on the red **History** button on the right of the appliance name.

So you have access to everything that has been executed on the appliance.

![Appliance history](img/history.png)
