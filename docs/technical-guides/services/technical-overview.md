---
title: Technical overview
---

The SentinelC platform supports embedding additional applications or services that can be packaged as linux containers.

Under the hood, the SentinelC OS uses the Podman container engine to run containers and pods. An application is mostly a podman/kubernetes compatible yaml manifest template and some extra metadata such as a name, description and input parameters.

The following diagram shows the main concepts involved in the services system.

![diagram](sentinelc-service-diagram.png)

- An admin registers a repository inside the SentinelC API.
- The API fetch service definitions from the appfeed and imports it
- A user uses the service definitions to install an app to an appliance
- The app installation script is sent to the appliance and installed using podman (container)
- The API creates a host (virtual Mac Address) for the app instance and
- A static IP address is assigned to the app on first start.
- The app can be accessed with the cloud proxy, the local ports or a web-based terminal depending on the settings chosen at installation.
