---
title: Blocking device network
---


## How block a device from accessing your network?

To **Block a device**:

Go to the **Devices** section, you will then see the list of devices that are or have connected to your network.

![Device list](./blocking-device-network/device-list.png)

Select the device you want to **block**.

![Device block](./blocking-device-network/device-block.png)

Scroll down to the bottom of the page to see the **Block device** button.

![Device blocked](./blocking-device-network/device-blocked.png)

Click **Block Device**, then click **Apply** to make the blocking effective. You must give a reason for blocking a device.

![Device blocked apply](./blocking-device-network/device-blocked-apply.png)

To **unblock** a device, go back to the page of the device that is blocked and select the device you want to **unblock**.

![Device unblock](./blocking-device-network/device-unblock.png)

Click **Unblock Device**, then click Apply to make the unblocking effective.

![Device unblock](./blocking-device-network/device-unblocked.png)