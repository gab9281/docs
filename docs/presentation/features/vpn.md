---
title: VPN

---

The VPN is very useful for the SentinelC application, it allows among other things:
- A management of the equipment.
- Remote access to machines even if there is a firewall or NAT.
- VPN machines connection .
- Interconnect two zones.

### A management of the equipment.