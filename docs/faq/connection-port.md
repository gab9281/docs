---
title: Connection port
hide_table_of_contents: true
---

import TOCInline from '@theme/TOCInline';


<TOCInline toc={
    toc
  } />

<div className="questions-title">

### Why don't I have Internet?

If your device is connected to the SentinelC router by an Ethernet network cable, check that your Ethernet network cable is not defective, use another cable if possible.

Check that your modem is connected to the router with an Ethernet cable.

Check that your modem is synchronized with the Internet. Contact your Internet Service Provider if you are having problems with your modem.

If the problem persists, please contact the technical support team.

### Why doesn't my Ethernet port turn on and work?

If your Ethernet port does not light up or flash, check the following

- Your network cable is properly connected.
- Your router, modem and devices are turned on.
- Test with another Ethernet cable

If the problem persists, please contact technical support.

### Which Ethernet port on the router should I connect my modem to?

For the SentinelC-APU3 model, see the data sheet.

The modem must be connected with an Ethernet network cable to the first port on the left side of the SentinelC router.

### Can I plug something other than an access point into an access point port?

No, you simply cannot plug anything other than an access point into an access point port.

If a device other than an access point is plugged into an access point port, it will not be able to access the Internet. In the SentinelC application, an error will appear indicating that a forbidden device is plugged into the access point port, and an authorization request will appear but it will be impossible to authorize the device on the network.

### I plugged an unauthorized device into an Access Point port.

If I plug an unauthorized device into an access point port, I will receive an authorization request with an error message that says this device cannot be authorized. In addition, a notification is triggered indicating a bad wire connection.

A device other than a SentinelC access point network device is not allowed to connect to an access point port. In the authorization request, the request can only be ignored and a new connection made to another port that is not an access point port.

### Why does the port my device was connected to appear in gray in the SentinelC application?

To know the status of the ports of your device, refer to the [Ports status](/docs/user-guides/dashboard/ports-status) page.

In the appliance's information page, at the bottom you will see the number of available and used ports, their status will appear depending on the current configuration. A legend appears to better describe their status.

### Why does my port appear Closed in the application?

To know the status of the ports of your device, refer to the [Ports status](/docs/user-guides/dashboard/ports-status) page.

This usually means a bad connection.

### My device is plugged into an access port and appears incorrectly plugged in the application, why?

To know the status of the ports of your device, refer to the [Ports status](/docs/user-guides/dashboard/ports-status) page.

A device connected to an access point port is a prohibited device, for the simple reason that it is not an access point device, so it cannot be connected to an access point port.

### How can I edit the ports of a device to assign them a zone?

To edit the ports of a device to assign them a zone, refer to the [Create zone](/docs/user-guides/dashboard/create-zone#edit-the-ports-of-an-appliance-to-assign-them-a-zone) page.

### I purchased a SentinelC access point. Which port on my main router should I plug it into?

You must plug your SentinelC access point into an "Access Point" port on the SentinelC core router. Refer to the [Installation acces point](/docs/user-guides/dashboard/installation-access-point) page.

### Is my device wired or Wi-Fi connected?

To see if your device is wired or Wi-Fi connected, go to the application, select your location and go to the Devices tab.

Under the connection date, you will see the type of connection: Wi-Fi or wired.

You can find this information in Device Information by selecting the device.

</div>