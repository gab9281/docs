---
title: What is the SentinelC project?
slug: /
---

The SentinelC router is a cybersecurity solution that protects your wireless networks at home or in the workplace. With this security router, it is possible to manage and administer your Internet network from a Mobile Web application or admin portal. 

The SentinelC project consists of the following main components:

- A security-first operating system for network appliances.
- A self-hosted, cloud controller for remote management.
- A full-featured, desktop and mobile web application for remote administration and monitoring.

Together, these components form a complete, security focused, network management solution that allows you to easily operate your home or work networks.

### Who is this for?

You don't have to be an IT professional to use the SentinelC solution, it has been thought user friendly!

Whether you are an individual or a small business owner, nothing is easier today than to manage your networks with the SentinelC.

#### Power users and network enthusiasts

To test and have a global view of your network, as well as a facility to manage it.

#### Small/medium business

To control network connections in the company for monitoring purposes and manage multiple locations.

#### IT services companies / Managed Service Providers (MSPs)

All your customers are on it, so you can manage them remotely without any problem.

#### Network security researchers

You have the possibility to develop security functions or to experiment on your networking equipment.


## Concept

The Tenant account is managed by an administrator. This account has 2 locations: Location 1 and Location 2. Location 1 has multiple Zones (VLAN) and each zone has one or more Wi-Fi or an Ethernet connection. Take the Camera zone as an example, this zone is supposedly restricted and therefore no other device can communicate outside of this zone. Only cameras can communicate with each other. 


![functional structure](./project-view---3.jpg)

## Solutions

SentinelC has the ability to monitor and manage the network and is a complete system that responds to different types of users: 
- A simple administrator:
He manages his network simply via SentinelC dashboard mobile.
The user can manage a small network and does not need to have advanced technical knowledge because there are no advanced monitoring management compared to the administrator portal.


He can then add zones, create Wi-Fi networks, see the connected devices, or block a device on his network. But this dashboard is a limited version compared to the administrator portal.

![Dashboard](./dashboard.jpg)

- A network operator:
It has the ability to monitor the network by performing for example packets capture or ports scan. it's platform to operate multiple networks.
The administrator can manage multiple accounts and locations and see everything that happens on separate networks.
It can manage and view incident details and perform monitoring tasks.

The application is secured by managing the access of a user. If the user is a local administrator, he will only see his own account and location. If the user is a privileged administrator, then he will be able to see all accounts and all locations. A privileged administrator will then be able to resolve incidents on the network. He can also revoke an administrator's access

![Admin portal](./admin-portal.jpg)


## Secure

Secure and protect your users with our SentinelC solution. This secure network management platform allows you to control your Wi-Fi and Ethernet networks and the devices that connect to them. 

- Communication vpn avec clé.
- Firmware update constant.
- Diagram à haut niveau avec cloud et vpn, mise à jour auto, etc...
- Comment securiser les wi-fi avec une partie fonctionnelle securitaire avec clé wifi aléatoire.
- Clé utilisé sur les appareils.
- Possibilité d'ajouter authentification double facteur (Keycloak).
- Partie produit:
    - Application avec login/mot de passe (Keycloak).
    - Surveillance d'appareils.
    - Gestion des pannes d'équipements
    - Gestion des administrateurs.
    - Gestion des incidents.

- Access / Privilege Management
    - Local administrator
        - Account and rental management.
        - Account administrator management.
        - Management of Wi-Fi, Ethernet and VLANS networks.
        - Device management
        - Add Master Router, AP or Drone.

    - Privileged administrator
        - Management of all accounts and locations.
        - Management of local and privileged administrators.
        - Device management.
        - Connection management.
        - Incident management.
        - Management of monitoring tasks.



## Monitor

SentinelC helps to monitor your internal and external network, by performing for example a Wi-Fi scan, a port scan or a packet capture.

![Surveillance](./surveillance.jpg)

SentinelC can help you observe and analyze traffic as well as incoming and outgoing connections on your network.

Our solution collects the number of data transferred in download and upload as well as the number of network connections

![Monitor](./monitor.jpg)

- Monitoring, analyse de traffic
- Monitoring icinga

## Configure

SentinelC Application allows the creation of zone, Wi-Fis, it also allows to edit the ports of an appliance to add a zone or a port type.

![Configure](./configure.png)

- Create SentinelC account.
an account is used by one or more administrators.
- Create location
One or more locations can be created for a single account.
- Create Wi-Fi
- Create Zone (VLAN)
- Add or delete Administrator


Petit diagramme avec admin qui possede un compte avec locations

## Manage

With SentinelC, you can manage your account, location, appliance inventory, perform firmware updates. 

![Manage](./manage.jpg)

You can manage user administrator.

It is possible to manage users and give them privileged access for multi user, and manage multi network.

![Users](./users.jpg)


## How it works?

1. A SentinelC router connected to the Internet.
2. Network segmentation into zone to group devices together.
3. Devices connected on different zone.


![SentinelC-Roadmap](./sentinelc-roadmap.jpg)

## Main features

- [Mobile and Desktop web application](presentation/features/webapp).
- [Easily segment your devices into isolated Zones](presentation/features/zones).
- [Manage WiFi networks](presentation/features/wifi).
- [Manage wired Ethernet devices and ports](presentation/features/wired).
- [Seamless, safe over-the-air firmware updates](presentation/features/ota-updates).
- [QoS](presentation/features/qos).
- [Isolate accounts](presentation/features/multi-tenancy).
- [Wireguard VPN](presentation/features/vpn).
- [Network security operations](presentation/features/secops).

## Getting started

You will need to following items in order to get started:

- A host for your [cloud controller](controller/intro).
- One or more compatible [network appliances](hardware/supported-hardware).
