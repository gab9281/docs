---
title: Appliance status
---


NEW = équipement d'usine qui se connecte pour la première fois au endpoint de l'environnement.

RETURNED = équipement non défectueux retourné à l'inventaire (opérateur) par un client.

IN STOCK = équipement qui a été ajouté à l'inventaire (préalablement à l'état NEW, RETURNED ou DEFECTIVE).

DEFECTIVE = équipement identifiés comme défectueux et qui a été retourné à l'inventaire (opérateur) par un client.

UNAVAILABLE = équipement qui a été transféré d'environnement pour une période de temps.

DISCARDED = équipement détruit logiquement de l'inventaire qui ne peut plus réintroduire l'inventaire.