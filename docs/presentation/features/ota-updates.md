---
title: Firmware updates

---

Lack of security updates and maintenance from network hardware vendors is a well known problem in the marketplace.

Even when manufacturers do provide security updates, they are often time cumbersome and risky to install by end-users. Users can also simply not be aware of available updates and the need for installing updates.

The SentinelC firmware is built from the ground up to be easy and safe to update.

### You are in control

- Full over-the-air (OTA) updates, centrally controlled from the admin portal.
- Install updates immediately or schedule them in the next maintenance window.

### Trust, but verify

- Official updates are cryptographically signed.
- The signature is verified on-device during the update process to detect any tampering.
- Advanced users and developers can fork, build and sign their own updates.

### Update with confidence

- Two versions are always available on disk.
- Updates are streamed to the inactive storage partition with no downtime, even on slow and unreliable networks.
- The system becomes unavailable only during the short time it takes to reboot into the new version.
- In the unlikely case the new version fails to initialize normally, an automated rollback to the previous version is performed.

## An in-depth look

Update binaries are distributed in the mender artifact format. The standalone [mender](https://mender.io/) utility is built into the firmware and is used to install and rollback updates.

The [cloud controller](/controller/intro.md) tracks installed versions, available updates and their distribution.

The firmware is split into 4 main parts:

- A small boot partition containing the critical parts required to boot the system.
- Two copies of the main system: A and B.
- A persistent data partition where persistent configurations can be preserved.

![Firmware high-level diagram](./firmware-highlevel.svg)
