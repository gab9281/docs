---
title: Dashboard overview
---

## Network view

When installing the SentinelC solution, you need to register a SentinelC account which will be associated with your appliances (Router, access point or drone), your networks and the users and devices authorized to them.

Let's take the example above:

An administrator has access to company A which has 2 locations A1 and A2. Each location has a Wi-Fi: AW1 and AW2.

EnterpriseNetwork
- Location Home
    - Wi-Fi Home
- Location Office
    - Wi-Fi Office
    - Wi-Fi Private 

There are 2 possible display modes for the dashboard, the standard mode:

![Dashboard](dashboard-overview/dashboard1.png)

And the advanced mode:

![Dashboard2](dashboard-overview/dashboard2.png)

You can switch at any time the mode via the user menu at the top right.

The administrator can then see the devices connected to the Wi-Fi.
He can also manage his [Wi-Fis](/docs/user-guides/dashboard/add-wi-fi) and [create zone](/docs/user-guides/dashboard/create-zone).


### Connected Devices

The application lists the devices on the network.

![Dashboard connected device](dashboard-overview/dashboard-connected-device.png)

### Activity

The user can see the activity on his network.

![Dashboard activity](dashboard-overview/dashboard-activity.png)

### Incident history

A page lists the incidents that occurred on the network.

![Dashboard incident history](dashboard-overview/dashboard-incident.png)

### Manage Appliance

The user can manage his appliance.

![Dashboard Manage Appliance](dashboard-overview/dashboard-manage-device.png)

### Manage Wi-Fi

The user can manage his Wi-Fi.

![Dashboard Manage Wi-Fi](dashboard-overview/dashboard-manage-wi-fi.png)

### Manage user access

The user can manage his user access.

![Dashboard Manage user](dashboard-overview/dashboard-manage-user.png)