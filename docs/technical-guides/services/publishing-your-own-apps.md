---
title: Packaging apps
---

It is possible to publish your own application library in order to run any application or service that can be deployed as one or more docker containers.

Application recipes are published in a JSON file format. You publish that JSON feed to an URL and subscribe your cloud controller instance(s) to it.

The raw JSON feed is not meant to be directly edited. Instead, a library of applications is maintained using a set of YAML files organized
in a git repository.

## The demo application feed

An example on how to organize and publish your own application feed is available in the [Demo App Library](https://gitlab.com/sentinelc/app-library-demo/) project.

The build and publishing of updates in this project is fully automated using Gitlab CI.

## The tooling and specifications

The full specifications on how to organize your application manifests is available in the [App Library Builder](https://gitlab.com/sentinelc/app-library-builder/) project.
