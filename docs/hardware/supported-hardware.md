---
title: Supported hardware

---

SentinelC OS currently supports different architectures including Intel-AMD and ARM.

## APU2

Technical specifications of the APU2 model:

- Architectures AMD.
- Compatibility BIOS.
- sd card
- Wireless connectivity: simultaneous dual-band 2.4GHz/5GHz Wi-Fi.
- Compatible with IEEE 802.11a/b/g/n/ac.
- Wired connectivity: 2 Ethernet LAN ports, 1 Ethernet WAN port.
- 2 × USB 3.0 ports.

## RPI4

Technical specifications of the RPI4 model B:

- Architectures ARM.
- Compatibility BIOS and UEFI.
- Wireless connectivity: simultaneous dual-band 2.4GHz/5GHz Wi-Fi and/or and dongle USB Wi-Fi canakit 2.4GHz.
- Compatible with IEEE 802.11a/b/g/n/ac.
- Wired connectivity: 1 Ethernet WAN port.
- 2 × USB 3.0 and 2 × USB 2.0 ports.
- 3 miniPCI express

## Lanner

Technical specifications of the Lanner model NCA 1510D:

- Architectures Intel.
- Compatibility BIOS and UEFI.
- Wired connectivity: 13 Ethernet LAN ports, 1 Ethernet WAN port.
- 1x Mini-PCIe, 1x M.2, 1x 2.5” HDD/SSD Bay

## Supermicro

Technical specifications of the Supermicro model SYS-E300

- Architectures Intel.
- Compatibility BIOS and UEFI.
- Wired connectivity: 7 Ethernet LAN ports, 1 Ethernet WAN port, 2 sfp fiber optic ports.
- 2 × USB 3.0
- 1 PCI-E 3.0 x8 AOC slot (LP), Mini PCI-E (mSATA support), M.2 PCI-E 3.0 x4.