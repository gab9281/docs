---
title: Web application

---

TODO

- Mobile optimized web app for day-to-day network management:
    - add zones.
    - add appliances.
    - add Wi-Fi networks.
    - Wi-Fi schedule management.
    - See connected devices.
    - See the activities on the network.
    - See incidents detected on the network.


- Desktop administration console:
    - Manage accounts and locations.
    - Manage all network devices.
    - Manage all network devices.
    - Manage all network connections.
    - Manage all incidents.
    - Enable packets capture.
    - Enable Wi-Fi scan.
    - Manage potential rogue Wi-Fi.
    - Enable port scan.
