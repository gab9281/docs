---
title: Limitations

---

## Advanced configurations / customizations

The SentinelC project is very opinionated. The range of configuration options is intentionally limited compared to other traditional networking systems.

One of the goal of the project is to prevent an administrator from deploying a bad or insecure configuration. In other words, there are no foot guns.

This means that the range of configuration is kept to a minimum. We do and still want the project to be useful for a broad range of use-cases.

If you encounter a missing feature, please open an issue to help improve the project.

## Interoperability with other networking gear

While we do use and support networking standards such as VLANs, DHCP, WPA2, etc., many of the features of the project requires control over all in-use networking equipments.

Mix and matching routers, switches and access points from other vendors will cause mixed results.

For example, extending wireless coverage by adding non-sentinelc access points to your network will generally work, but you will loose monitoring and security features that a fully compatible access point could provide.

## Offline support (cloud controller required)

SentinelC appliances offer only the very bare minimum local administration. All configuration and monitoring is meant to happen using the cloud controller. This is by design.

Configurations are cached locally, and monitoring events are alse queued locally. This allows the appliances to keep chugging along in the case of a short cloud-controller outage or network connectivity loss.

That being said, the system is not meant to be operated for long periods of time without an available cloud controller.

## Hardware support

The SentinelC OS targets general purpose computing platforms.

Very low cost, constrained hardware platforms might never be supported.

We are always looking to support new interesting hardware. Being Linux based, porting to new hardware is quite flexible.
