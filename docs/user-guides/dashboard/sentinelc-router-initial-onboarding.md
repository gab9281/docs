---
title: SentinelC router initial onboarding

---

## Onboarding

### Step 1: Go to the web application.

Go to https://app.sentinelc.com with your smartphone and click on **Start installation**.

Enter the product key when prompted.

The key is printed on the back of the Quick Setup Guide and on the bottom of your SentinelC router. 

![step1](./appliance-onboarding/step1.jpg)

### Step 2: Registration.

Complete your registration with the account name and contact. 

![step2](./appliance-onboarding/step2.jpg)

### Step 3: Administrator profile.

Complete the creation of the administrator profile.

![step3](./appliance-onboarding/step3.jpg)

### Step 4: Location.

Complete the creation of the location and the Wi-Fi network.

![step4](./appliance-onboarding/step4.jpg)

Create a Wi-Fi network name, a network password will be generated automatically.

![step4bis](./appliance-onboarding/step4bis.jpg)


### Step 5: Configuration summary.

Check the information entered in steps 1, 2, 3 and 4.

![step5](./appliance-onboarding/step5.jpg)

Finally click on Finish to saving your network configuration.

![step5bis](./appliance-onboarding/step5bis.jpg)

### Step 6: Email confirmation.

A confirmation message will appear and all you have to do is validate your account by **email**. 

From the email you receive, click on the link that will direct you to the application. 

You will then be prompted to create your **password**. Then click **Submit** to confirm the creation. A message will appear, your account has been updated, click on **Log in to the application now**. 

Finally, log in to your account with your email and password created earlier.

### Step 7: Connection with the modem.

Now it's time to connect your SentinelC router to your modem.
For the SentinelC-APU2 model, [see data sheet](https://www.sentinelc.com/faq/sentinelc-a3p/).
It is now time to connect your SentinelC router to your modem.

### Step 8: Finalize installation.
To finalize the installation, return to : [https://app.sentinelc.com](https://app.sentinelc.com). The installation starts automatically if the router is properly connected.
Note: If the SentinelC router is not up to date, the automatic update download is started. When the installation is complete, click on "Go to application" to access the dashboard.