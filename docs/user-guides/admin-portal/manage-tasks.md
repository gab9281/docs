---
title: Manage tasks
---

## How to view tasks on your portal admin?

The tasks page of the Admin portal allows you to see the tasks that are performed on SentinelC devices.

Several tasks such as packet capture, Wi-Fi scan, firmware update or device reset or reboot.

![Manage-tasks](manage-tasks/manage-tasks.png)
