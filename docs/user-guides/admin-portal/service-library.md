---
title: Service library
---

The service library contains recipes used to launch [embedded services](/docs/user-guides/admin-portal/manage-appliances/embedded-services/).

## How to manage services from your library
You can access the library of services using the service tab on the admin page (gear icon).


This page shows you all usable services and can be filtered by repository, category or status.
You can manually disable a service from a repository by using the checkbox on the left then use the action selector to disable it.
Disabled services are no longer visible in the "add a service" windows of an appliance.

![Manage-accounts](manage-services/manage-services-enable.png)

## How to monitor services

To monitor the currently running services, click on the name of the service.
All the informations shown to the user will be available and every installed instances of the program will be accessible.

![Manage-service](manage-services/service-library-info.png)
