## SentinelC documentation

This repository contains the source of the documentation website available at https://docs.sentinelc.com/

If you are looking for the documentation itself, go directly to the built documentation site at: https://docs.sentinelc.com/

The rest of this document covers how to improve the documentation.

## Contributing to the documentation

Improvements and contributions to this documentation are welcomed.

Follow these steps to get started.


### Create your fork

We recommend to first create your personal Fork of the public repository using your personal namespace.

https://gitlab.com/sentinelc/docs/-/forks/new

You can make your fork public or private. If your fork is private, only members of your forked project (you) will have access to preview your changes.

You might want to name your fork `sentinelc-docs` instead of the generic default `docs`.

### Configure BASE_URL

To test your fork using Gitlab pages, you will need to change the `BASE_URL` ci/cd setting to match your project name.

If your forked project is named `sentinelc-docs`, set the BASE_URL to `/sentinelc-docs/`.

Variables are defined under Settings / CI/CD / Variables.


### Create a branch and start working

Create a branch and start making changes. If you use the Web IDE, it will suggest to create a branch when you commit your changes.

All push to any branches will be deployed to your gitlab page preview site.

The URL is visible under Settings / Pages and should look like:

https://[username].gitlab.io/sentinelc-docs/

You might want to enable HTTPS under Settings / Pages.


## Local Development

First time install:

```console
npm install
```

Start the local server:

```console
npm run start
```

This command starts a local development server and open up a browser window. Most changes are reflected live without having to restart the server.
